﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace QiwiSteamPayer {
	internal static class Program {
		private static HttpClient HttpClient;

		private static void Log(string log) {
			Console.WriteLine(log);
			File.AppendAllText("log.txt", log + Environment.NewLine);
		}

		private static async Task Main() {
			string token;
			decimal sum = 0;
			if (File.Exists("log.txt")) {
				File.Delete("log.txt");
			}

			if (File.Exists("settings.txt")) {
				string[] settings = File.ReadAllLines("settings.txt");
				token = settings[0];
				if (!((settings.Length > 1) && decimal.TryParse(settings[1].Replace(',', '.'), NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out sum) && (sum > 0))) {
					Console.WriteLine("Sum was not found or it is incorrect, please enter it:");
					while (sum <= 0) {
						decimal.TryParse(Console.ReadLine(), out sum);
					}
				}
			} else {
				Console.WriteLine("Settings file was not found, please enter token:");
				token = Console.ReadLine();
				Console.WriteLine("Please enter sum:");
				while (sum <= 0) {
					decimal.TryParse(Console.ReadLine(), out sum);
				}
			}

			HttpClient = new HttpClient {
				DefaultRequestHeaders = {
					Accept = {
						MediaTypeWithQualityHeaderValue.Parse("application/json")
					},
					Authorization = AuthenticationHeaderValue.Parse($"Bearer {token}")
				}
			};

			List<string> accounts = null;
			if (File.Exists("accounts.txt")) {
				accounts = File.ReadAllLines("accounts.txt").ToList();
			} else {
				Console.WriteLine("File with accounts was not found, please enter accounts joining them by space:");
				while (accounts == null) {
					accounts = Console.ReadLine()?.Split(' ').ToList();
				}
			}

			bool firstIteration = true;
			foreach (string account in accounts) {
				if (firstIteration) {
					firstIteration = false;
				} else {
					await Task.Delay(15000).ConfigureAwait(false);
				}

				try {
					SendMoneyRequest sendMoneyRequest = new SendMoneyRequest(sum, account);
					HttpResponseMessage responseMessage = await HttpClient.PostAsync("https://edge.qiwi.com/sinap/api/v2/terms/25549/payments",
						new StringContent(JsonConvert.SerializeObject(sendMoneyRequest), Encoding.UTF8, "application/json")).ConfigureAwait(false);
					if (!responseMessage.IsSuccessStatusCode) {
						Log($"[{account}] Error: " + responseMessage.StatusCode);
						continue;
					}

					string responseString = await responseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);
					if (string.IsNullOrEmpty(responseString)) {
						Log($"[{account}] Error: content is empty");
						continue;
					}

					dynamic responseJson;
					try {
						responseJson = JsonConvert.DeserializeObject(responseString);
					} catch (Exception) {
						Log($"[{account}] Error: failed to parse response");
						continue;
					}

					string responseCode = (string) responseJson.transaction?.state?.code;
					if (string.IsNullOrEmpty(responseCode)) {
						Log($"[{account}] Error: invalid response");
						continue;
					}

					if (responseCode != "Accepted") {
						Log($"[{account}] Error: " + responseCode + "|" + responseString);
						continue;
					}

					Log($"[{account}] Done!");
				} catch (Exception ex) {
					Log($"[{account}] Error: " + ex.Message);
				}
			}

			Console.WriteLine("Press any key to close...");
			Console.ReadKey();
		}

		internal class SendMoneyRequest {
			private static readonly SendMoneyRequestPaymentMethod SendMoneyRequestPaymentMethodInstance = new SendMoneyRequestPaymentMethod();

			[JsonProperty(PropertyName = "fields", Required = Required.Always)]
			private readonly SendMoneyRequestFields Fields;

			[JsonProperty(PropertyName = "id", Required = Required.Always)]
			private readonly string ID;

			[JsonProperty(PropertyName = "paymentMethod", Required = Required.Always)]
			private readonly SendMoneyRequestPaymentMethod PaymentMethod;

			[JsonProperty(PropertyName = "sum", Required = Required.Always)]
			private readonly SendMoneyRequestSum Sum;

			internal SendMoneyRequest(decimal amount, string accountName) {
				Fields = new SendMoneyRequestFields(accountName);
				ID = (DateTimeOffset.Now.ToUnixTimeSeconds() * 1000).ToString();
				PaymentMethod = SendMoneyRequestPaymentMethodInstance;
				Sum = new SendMoneyRequestSum(amount);
			}

			private class SendMoneyRequestSum {
				[JsonProperty(PropertyName = "currency", Required = Required.Always)]
				internal const string Currency = "643";

				[JsonProperty(PropertyName = "amount", Required = Required.Always)]
				internal decimal Amount;

				internal SendMoneyRequestSum(decimal amount) => Amount = amount;
			}

			private class SendMoneyRequestPaymentMethod {
				[JsonProperty(PropertyName = "accountId", Required = Required.Always)]
				internal const string AccountID = "643";

				[JsonProperty(PropertyName = "type", Required = Required.Always)]
				internal const string Type = "Account";
			}

			private class SendMoneyRequestFields {
				[JsonProperty(PropertyName = "account", Required = Required.Always)]
				internal string AccountName;

				internal SendMoneyRequestFields(string accountName) => AccountName = accountName;
			}
		}
	}
}
